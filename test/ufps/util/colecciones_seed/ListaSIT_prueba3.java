/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import java.awt.BorderLayout;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Javier
 */
public class ListaSIT_prueba3 {

    public ListaSIT_prueba3() {
    }

    @Test
    public void testRemove() {
        ListaS<Integer> lista = new ListaS();
        lista.insertarAlInicio(203);
        lista.insertarAlInicio(123);
        lista.insertarAlInicio(956);
        lista.insertarAlInicio(432);
        lista.insertarAlInicio(864);
        lista.insertarAlInicio(123);
        lista.insertarAlInicio(564);
        lista.insertarAlInicio(568);
        lista.insertarAlInicio(325);

        Integer random = lista.get(5);
        long e = System.nanoTime();
        Integer eliminado = lista.eliminar(5);
        long demora = System.nanoTime() - e;
        System.out.println("La eliminacion se demoro: " + demora + " nanosegundos");

        assertTrue(random.equals(eliminado));
    }

    @Test
    public void testRemoveNumerosAleatorios() {
        ListaS<Integer> lista = new ListaS();
        for (int i = 0; i < 20; i++) {
            lista.insertarAlFinal((int) (Math.random() * 1000));
        }

        Integer random = lista.get(13);
        long e = System.nanoTime();
        Integer eliminado = lista.eliminar(13);
        long demora = System.nanoTime() - e;
        System.out.println("La segunda eliminacion se demoro: " + demora + " nanosegundos");

        assertTrue(random.equals(eliminado));
    }

    @Test
    public void testEquals() {
        ListaS<Integer> lista = new ListaS();
        lista.insertarAlFinal(203);
        lista.insertarAlFinal(123);
        lista.insertarAlFinal(956);
        lista.insertarAlFinal(432);
        lista.insertarAlFinal(864);

        ListaS<Integer> lista2 = new ListaS();
        lista2.insertarAlFinal(203);
        lista2.insertarAlFinal(123);
        lista2.insertarAlFinal(956);
        lista2.insertarAlFinal(432);
        lista2.insertarAlFinal(864);

        long e = System.nanoTime();
        boolean equals = lista.equals(lista2);
        long demora = System.nanoTime() - e;
        System.out.println("El primer metodo equals demoró: " + demora + " noanosegundos");

        assertTrue(equals);
    }

    @Test
    public void testEquals2() throws ExceptionUFPS {
        ListaS<Integer> lista = new ListaS();
        lista.insertarAlFinal(345);
        lista.insertarAlFinal(387);
        lista.insertarAlFinal(908);
        lista.insertarAlFinal(567);
        lista.insertarAlFinal(123);

        ListaS<Integer> lista2 = new ListaS();
        lista2.insertarAlFinal(345);
        lista2.insertarAlFinal(387);
        lista2.insertarAlFinal(908);
        lista2.insertarAlFinal(567);
        lista2.insertarAlFinal(123);

        long e = System.nanoTime();
        boolean equals = lista.equals(lista2);
        long demora = System.nanoTime() - e;
        System.out.println("El segundo metodo equals demoro: " + demora + " nanosegundos");

        assertTrue(equals);
    }

    @Test
    public void testaVector() {
        ListaS<Integer> lista = new ListaS<>();
        Integer[] listaI = new Integer[20];
        int j = 0;
        for (int i = 0; i < 20; i++) {
            int n = (int) (Math.random() * 1000);
            lista.insertarAlFinal(n);
            listaI[j++] = n;
        }
        long e = System.nanoTime();
        Object[] ListaI2 = lista.aVector();
        long demora = System.nanoTime() - e;
        System.out.println("El primer test aVector: " + demora + " nanosegundos");
        assertArrayEquals(listaI, ListaI2);
    }

    @Test
    public void testaVector2() {
        ListaS<Integer> lista = new ListaS<>();
        Integer[] vector = new Integer[10];
        int j = 9;
        for (int i = 0; i < 10; i++) {
            int x = i * 2;
            lista.insertarAlInicio(x);
            vector[j--] = x;
        }
        long e = System.nanoTime();
        Object[] vector1 = lista.aVector();
        long demora = System.nanoTime() - e;
        System.out.println("El segundo aVector demoró: " + demora + " nanosegundos");
        assertArrayEquals(vector, lista.aVector());
    }

    @Test
    public void ordenarInsercionPorNodos() throws ExceptionUFPS {
        ListaS<Integer> lista = new ListaS<>();
        ListaS<Integer> ordenada = new ListaS<>();
        
        lista.insertarAlInicio(325);
        lista.insertarAlInicio(3124);
        lista.insertarAlInicio(7667);
        lista.insertarAlInicio(56);
        lista.insertarAlInicio(6787);

        ordenada.insertarOrdenado(325);
        ordenada.insertarOrdenado(3124);
        ordenada.insertarOrdenado(7667);
        ordenada.insertarOrdenado(56);
        ordenada.insertarOrdenado(6787);
        
        long e = System.nanoTime();
        lista.ordenarInsercion_Por_Nodos();
        long time = System.nanoTime() - e;
        System.out.println("El ordenamiento por insercion de nodos demoró: " + time + " nanosegundos");
        assertArrayEquals(lista.aVector(), ordenada.aVector());
    }

    @Test
    public void ordenarInsercionPorNodos2() throws ExceptionUFPS {
        ListaS<Integer> lista = new ListaS<>();
        ListaS<Integer> ordenada = new ListaS<>();

        for (int i = 0; i < 100; i++) {
            int n = i * 10 / 3 * 2;
            lista.insertarAlFinal(n);
            ordenada.insertarAlInicio(n);
        }
        long e = System.nanoTime();
        lista.ordenarInsercion_Por_Nodos();
        long time = System.nanoTime() - e;
        System.out.println("El segundo ordenamiento por insercion de nodos demoró: " + time + " nanosegundos");
        ordenada.ordenarInsercion_Por_Nodos();

        assertArrayEquals(lista.aVector(), ordenada.aVector());
    }
}
